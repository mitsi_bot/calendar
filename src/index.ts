import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
import express, { Response, Request } from "express";
import calendar from "./calendar";

class Server extends BaseService {
    onPost(request: Request, response: Response): void {
        calendar(request.body as ServiceInput)
            .then((res: ResponseText) => {
                response.json(res);
            }).catch((err: Error) => {
                console.error(err);
                response.json(new ResponseText("Calendar error"));
            });
    }
}

new Server(express()).startServer();
