import { AxiosResponse } from 'axios';
import { destructDate } from 'mitsi_bot_lib/dist/src/Helper';
import ServiceInput from 'mitsi_bot_lib/dist/src/ServiceInput';
import ResponseText from 'mitsi_bot_lib/dist/src/ResponseText';
const fs = require("fs");
const readline = require("readline");
const { google } = require("googleapis");
const config = require('config-yml');

interface CalendarEvent {
  end: {
    date: Date
    dateTime: Date
  },
  start: {
    date: Date
    dateTime: Date
  },
  summary: string
}

// If modifying these scopes, delete token.json.
const SCOPES = ["https://www.googleapis.com/auth/calendar.readonly"];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = "credentials/token_calendar.json";

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials: any, callback: Function) {
  const { client_secret, client_id, redirect_uris } = credentials.web;
  const oAuth2Client = new google.auth.OAuth2(
    client_id,
    client_secret,
    "http://localhost:9001" //redirect_uris[0]
  );

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err: Error, token: string) => {
    if (err) return getAccessToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getAccessToken(oAuth2Client: any, callback: Function) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: "offline",
    scope: SCOPES,
  });
  console.log("Authorize this app by visiting this url:", authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question("Enter the code from that page here: ", (code: string) => {
    rl.close();
    oAuth2Client.getToken(code, (err: Error, token: string) => {
      if (err) return console.error("Error retrieving access token", err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err: Error) => {
        if (err) return console.error(err);
        console.log("Token stored to", TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

/**
 * Lists the next 10 events on the user's primary calendar.
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
const listEvents = (resolve: Function) => (auth: any) => {

  const calendar = google.calendar({ version: "v3", auth });
  calendar.events.list(
    {
      calendarId: "primary",
      timeMin: new Date().toISOString(),
      maxResults: 10,
      singleEvents: true,
      orderBy: "startTime",
    },
    (err: Error, res: AxiosResponse) => {
      if (err) {
        const response = "The API returned an error: " + err;
        resolve(new ResponseText(response));
        return console.log(response);
      }
      const events = res.data.items;
      let listOfEvent = [];
      if (events.length) {
        listOfEvent.push("Voilà tes prochains rdv :");
        events.map((event: CalendarEvent) => {
          const startDate = destructDate(new Date(event.start.dateTime || event.start.date));
          const endDate = destructDate(new Date(event.end.dateTime || event.end.date));
          listOfEvent.push(`${startDate.days}/${startDate.months}/${startDate.years} ${startDate.hours}:${startDate.minutes} - ${endDate.days}/${endDate.months}/${endDate.years} ${endDate.hours}:${endDate.minutes}\n\t\t\t\t${event.summary}`);
        });
      } else {
        listOfEvent.push(
          "Soit t'as rien à faire, soit j'ai pas réussi à savoir !"
        );
      }
      const response =
        listOfEvent.length == 1
          ? `${listOfEvent[0]}`
          : `${listOfEvent[0]}\n${listOfEvent
            .slice(1)
            .map((e) => `> ${e}`)
            .join("\n")}`;
      resolve(new ResponseText(response));
    }
  );
};

export default function calendar(result: ServiceInput): Promise<ResponseText> {
  return new Promise((resolve, reject) => {
    fs.readFile(config.app.pathToGooGleAuth, (err: Error, content: string) => {
      if (err) {
        reject(new ResponseText("Authentication file does not exists"));
        console.log("Error loading client secret file:", err);
      }
      authorize(JSON.parse(content), listEvents(resolve));
    });
  });
}

module.exports = calendar;
