FROM node:12.16-slim

WORKDIR /server

COPY . /server
RUN npm install && npm run compile

EXPOSE 9001
CMD [ "npm", "start" ]